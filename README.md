# Git Commit Words

This is an sample Express API application using MongoDB (via `mongoose`) and Git (via `nodegit`).

## Prerequisites

Due to the use of some ES2015 features (Promise, Set, Array.from, Arrow functions), this application
requires requires Node 6+

An accessible MongoDB server must be available.

## Installation

The source can be cloned from bitbucket and dependencies installed via npm.

```bash
git clone https://bitbucket.org/tekerson/git-commit-words git-commit-words
cd git-commit-words
npm install
```

## Usage

### Starting the API service

Once the dependencies have been installed, the server can be run with default configuration by
using the npm start script.

```bash
npm start
```

### Configuration

There are a some environment variables that can be set to configure the service.

 * `PORT` (default "3000") - The port number that the HTTP API server will listen on
 * `DB_URI` (default "mongodb://localhost/git-commit-words") -
   [MongoDB Connection String](https://docs.mongodb.com/manual/reference/connection-string/) where
     the database can be found

### Endpoints

The service defines 2 endpoints;

#### POST /storeRepoDetails

Expects a `application/x-www-form-urlencoded` or `application/json` payload with a field named
`repository` that should be the location for a repository to fetch, extract, and store commit
information about.

#### GET /wordsForAuthors

Expects a query string parameter named `author` that contains the an email address. All words
used in commit messages by this author are returned.

### Examples

Here are some example cURL command line commands to demonstrate the API usage.

Add the commits from this repository to the database

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" \
     -d 'repository=https://bitbucket.org/tekerson/git-commit-words.git' \
     "http://localhost:3000/storeRepoDetails"
```

Get all the words "brenton@tekerson.com" has used in commit messages

```bash
curl -X GET "http://localhost:3000/wordsForAuthors?author=brenton@tekerson.com"
```

## Limitations and Considerations

 * Only commits in the history of the `master` branch are ingested. This could be improved by
   iterating every branch in the repository and fetching their history also.
 * There is no artificial limitation on the size of a repository. A denial of service could be
   a potential issue if a very large repository was cloned. This could be improved by adding some
   limitation, such as the length of checkout and analysis time or a maximum disk space.
 * The the authors' words are extracted from their commit messaged when they are requested.
   Depending on the traffic profile, it may be advantageous to perform processing this as
   repositories are ingested.
 * The local repositories are deleted once their commits are stored in the database. Again,
   depending on the traffic profile it may be better to keep the repositories and perform a
   `git fetch` to get the newest commits instead of cloning the repository every time.
 * A "word" is a token as parsed by the
   [Treebank Tokenizer](http://www.cis.upenn.edu/~treebank/tokenization.html) as implemented in the
   `natural` npm package. Depending on the purpose of the analysis, a more sophisticated definition
   may be required. For example, for some uses it may be useful to perform stem normalization or
   remove stop-words.
 * Untested cross-platform. This has only been tested on OS X with node v6.x. While there are no
   known compatibility issues, no effort has been made to ensure compatibility with other operating
   systems or JavaScript engines.
