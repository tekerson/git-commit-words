const E_DUPLICATE_KEY = 11000 // Error code from MongoDB indicating a duplicate key

const { Clone: clone } = require('nodegit')
const { withTempDirectory } = require('./utils')
const { TreebankWordTokenizer } = require('natural')
const { tokenize } = new TreebankWordTokenizer()

/**
 * Read all of the commits from the repositories master branch
 * @sig Repository -> Promise (Array Commit)
 */
const getHistory = repo => repo.getMasterCommit()
      .then(master => new Promise((resolve, reject) => {
        const history = master.history()
        history.on('end', resolve)
        history.on('error', reject)
        history.start()
      }))

/**
 * Save the relevant details from the commits in the database.
 *
 * Uses the commit's SHA1 as the database ID to prevent duplicates datbase entries. So,
 * duplicate key insertion errors are ignored, but other errors are propagated.
 *
 * @sig (CommitModel, Array Commit) -> Array (Promise Void)
 */
const saveToDatabase = (CommitModel, commits) => commits.map(commit => CommitModel.create({
  _id: commit.sha(),
  date: commit.date(),
  author: commit.author().email(),
  message: commit.message()
}).catch(err => {
  if (err.code === E_DUPLICATE_KEY) return // ignore failed inserts - duplicate keys
  throw err // propagate other errors
}))

/**
 * Extract the individual words from the commits' messages
 *
 * Also, removes "words" containing numbers or *only* non-alpha (eg. ":-)"). This is an
 * arbitrary decision and would depend on actual use-case.
 * @sig Array CommitRecord -> Set String
 */
const extractWords = commits => commits.reduce((words, commit) => new Set([
  ...words,
  ...tokenize(commit.message.toLowerCase())
    .filter(word => !(word.match(/\d/) || word.match(/^[^a-z]*$/)))
]), new Set())

module.exports = CommitModel => ({
  /**
   * Clone the repository (which is passed in the request) to a temporary location on the local
   * file system, extract details about its commits and store them in the MongoDB database before
   * returning a result indicating if it was successful
   * @sig (Request, Response) -> Void
   */
  storeRepoDetails (req, res) {
    if (req.body.repository == null) {
      res.status(400).json({ success: false, error: 'Missing required `repository` parameter' })
      return
    }
    withTempDirectory(tmp => clone(req.body.repository, tmp, { bare: true }).then(getHistory))
      .then(commits => Promise.all(saveToDatabase(CommitModel, commits)))
      .then(() => { res.status(201).json({ success: true }) })
      .catch(error => { res.status(500).json({ success: false, error }) })
  },

  /**
   * Search for all commits by the given author (by email, passed in the request) and extract the
   * set of words found in them, returning them as the response
   * @sig (Request, Response) -> Void
   */
  wordsForAuthors (req, res) {
    if (req.query.author == null) {
      res.status(400).json({ success: false, error: 'Missing required `author` parameter' })
      return
    }
    CommitModel.find({ author: req.query.author }).exec()
      .then(extractWords)
      .then(words => { res.json(Array.from(words)) })
      .catch(error => { res.status(500).json({ error }) })
  }
})
