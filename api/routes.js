const express = require('express')

module.exports = (controller) => {
  const router = express.Router()

  router
    .route('/storeRepoDetails')
    .post(controller.storeRepoDetails)

  router
    .route('/wordsForAuthors')
    .get(controller.wordsForAuthors)

  return router
}
