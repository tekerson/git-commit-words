const tmp = require('tmp')

/**
 * Create a temporary directory and ensure that it is cleaned up after the handler completes
 * @sig (tmpDir: String -> a) -> Promise a
 */
exports.withTempDirectory = (handler) => new Promise((resolve, reject) => {
  tmp.dir({ unsafeCleanup: true }, (err, tmpDir, cleanup) => {
    if (err) {
      reject(err)
      return
    }

    const result = handler(tmpDir)
          .then(
            (result) => {
              cleanup()
              return result // propagate success
            },
            (error) => {
              cleanup()
              throw error // propagate failure
            })
    resolve(result)
  })
})
