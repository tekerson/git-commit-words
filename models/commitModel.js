const { Schema } = require('mongoose')

const commitSchema = new Schema({
  _id: String,
  date: Date,
  author: String,
  message: String
}, {
  _id: false
})

module.exports = connection => connection.model('Commit', commitSchema)
