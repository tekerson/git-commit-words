const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const CommitModel = require('./models/commitModel')
const Controller = require('./api/controller')
const Routes = require('./api/routes')

const port = process.env.PORT || 3000
const dbUri = process.env.DB_URI || 'mongodb://localhost/git-commit-words'

mongoose.Promise = Promise // Use native promises
const connection = mongoose.connect(dbUri)

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/', Routes(Controller(CommitModel(connection))))

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
